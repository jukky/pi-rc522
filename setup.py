#!/usr/bin/env python

import os
from os.path import dirname, join
import sys
from setuptools import setup, find_packages
from setuptools.command.test import test as TestCommand


def read(*args):
    return open(join(dirname(__file__), *args)).read()

exec(read('pirc522', 'version.py'))

setup(
    name='pi-rc522',
    packages=find_packages(),
    include_package_data=True,
    version=__version__,
    description='Raspberry Pi Python library for SPI RFID RC522 module.',
    long_description='Raspberry Pi Python library for SPI RFID RC522 module.',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Topic :: Software Development',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
    ],
    author='wumpitz',
    url='https://gitlab.com/jukky/pi-rc522',
    license='GPLv2',
    install_requires=['spidev', 'RPi.GPIO', 'pycrypto'],
)
